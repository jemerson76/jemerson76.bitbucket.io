#!/usr/bin/env swift

import Foundation

private let fileManager = FileManager.default
private let currentDirPath = fileManager.currentDirectoryPath

struct Feature {
    let title: String
    
    init(title: String) {
        self.title = title
    }
    
    static var features: [Feature]? {
        
        do {
            let urlForPath = URL(fileURLWithPath: currentDirPath)
            let items = try fileManager.contentsOfDirectory(at: urlForPath, includingPropertiesForKeys: nil, options: .skipsHiddenFiles)

            let features = items.filter { (item) in
                item.hasDirectoryPath
                }.map { Feature(title: $0.lastPathComponent)
            }
            return features.count > 0 ? features.sorted { $0.title < $1.title } : nil
            
        } catch {
            print(error.localizedDescription)
            return nil
        }
    }
    
    static var listItems: String? {
        var htmlText = ""
        
        guard let features = features else { return nil }
        
        features.forEach { (feature) in
            htmlText += "<li><a href=\"./\(feature.title)/screenshots.html\">\(feature.title)</a></li>"
        }
        return htmlText
    }
    
}

struct Product {
    let title: String
    
    init(title: String) {
        self.title = title
    }
    
    static func getProducts(for feature: Feature) -> [Product]? {
        
        do {
            let urlForPath = URL(fileURLWithPath: "\(currentDirPath)/\(feature.title)")
            let items = try fileManager.contentsOfDirectory(at: urlForPath, includingPropertiesForKeys: nil, options: .skipsHiddenFiles)
            
            let products = items.filter { (item) in
                item.hasDirectoryPath
                }.map { Product(title: $0.lastPathComponent)
            }
            return products.sorted { $0.title < $1.title }
            
        } catch {
            print(error.localizedDescription)
            return nil
        }
    }
    
    func getHTMLText(for product: Product, htmlSnippet: String) -> String {
        return "<tr> <th colspan=\"5\"> <span class=\"appName\">\(product.title)</span> </th> </tr><tr> <td>\(htmlSnippet)</td></tr>"
    }
}

struct Screenshot {
    let filename: String
    
    init(filename: String) {
        self.filename = filename
    }
    
    private var filenameWithoutExtension: String {
        let fileURL = URL(fileURLWithPath: self.filename)
        return fileURL.deletingPathExtension().lastPathComponent
    }
    
    static func getScreenshots(for feature: Feature, product: Product) -> [Screenshot]? {
        
        do {
            let urlForPath = URL(fileURLWithPath: "\(currentDirPath)/\(feature.title)/\(product.title)")
            let items = try fileManager.contentsOfDirectory(at: urlForPath, includingPropertiesForKeys: nil, options: .skipsHiddenFiles)
            
            let screenshots = items.filter { (item) in
                item.lastPathComponent.lowercased().hasSuffix(".png") || item.lastPathComponent.lowercased().hasSuffix(".jpg")
                }.map { Screenshot(filename: $0.lastPathComponent)
            }
            return screenshots.sorted { $0.filename < $1.filename }
            
        } catch {
            print(error.localizedDescription)
            return nil
        }
    }
    
    func getHTMLText(for product: Product, screenshot: Screenshot) -> String {
        return "<a href=\"./\(product.title)/\(screenshot.filename)\" target=\"_blank\" class=\"screenshotLink\"> \(screenshot.filenameWithoutExtension)<br/> <img class=\"screenshot\" src=\"./\(product.title)/\(screenshot.filename)\" style=\"width: 100%;\" data-counter=\"1\"/> </a>"
    }
}

private func setupIndexHTML(with listItems: String) -> String {
    return "<!DOCTYPE html><html><head><title>iOS Screenshots By Feature</title><meta charset=\"UTF-8\"><link rel=\"stylesheet\" type=\"text/css\" href=\"styles.css\"></head><body><h1 class=\"feature\">Features</h1><ul>\(listItems)</ul></body></html>"
}

private func setupScreenshotsHTML(for feature: Feature, text: String) -> String {
    return "<!DOCTYPE html><html> <head> <title>\(feature.title)</title> <meta charset=\"UTF-8\"> <link rel=\"stylesheet\" type=\"text/css\" href=\"../styles.css\"> </head> <body class=\"medium\"> <h1 class=\"feature\">\(feature.title)</h1><p>Image size: <a href=\"javascript:document.body.className='small';\">small</a> | <a href=\"javascript:document.body.className='medium';\">medium</a> | <a href=\"javascript:document.body.className='large';\">large</a></p> <hr> <table> \(text) </table> <div id=\"overlay\"> <img id=\"imageDisplay\" src=\"\" alt=\"\"/> <div id=\"imageInfo\"></div></div><script type=\"text/javascript\">var overlay=document.getElementById('overlay'); var imageDisplay=document.getElementById('imageDisplay'); var imageInfo=document.getElementById('imageInfo'); var screenshotLink=document.getElementsByClassName('screenshotLink'); function doClick(el){if (document.createEvent){var evObj=document.createEvent('MouseEvents', true); evObj.initMouseEvent(\"click\", false, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null); el.dispatchEvent(evObj);}else if (document.createEventObject){//IE var evObj=document.createEventObject(); el.fireEvent('onclick', evObj);}}for (index=0; index < screenshotLink.length; ++index){screenshotLink[index].addEventListener('click', function(e){e.preventDefault(); var img=e.target; if (e.target.tagName=='A'){img=e.target.children[0];}// beautify var tmpImg=new Image(); tmpImg.src=img.src; imageDisplay.style.height='auto'; imageDisplay.style.width='auto'; imageDisplay.style.paddingTop='20px'; if (window.innerHeight < tmpImg.height){imageDisplay.style.height=document.documentElement.clientHeight+'px';}else if (window.innerWidth < tmpImg.width){imageDisplay.style.width=document.documentElement.clientWidth;+'px';}else{imageDisplay.style.paddingTop=parseInt((window.innerHeight - tmpImg.height) / 2)+'px';}imageDisplay.src=img.src; imageDisplay.alt=img.alt; imageDisplay.dataset.counter=img.dataset.counter; imageInfo.innerHTML='<h3>'+img.alt+'</h3>'; imageInfo.innerHTML +=img.src.split(\"/\").pop(); imageInfo.innerHTML +='<br/>'+tmpImg.height+'&times;'+tmpImg.width+'px'; overlay.style.display=\"block\";});}imageDisplay.addEventListener('click', function(e){e.stopPropagation(); // ! overlay.style.display=\"none\"; img_counter=parseInt(e.target.dataset.counter) + 1; try{link=document.body.querySelector('img[data-counter=\"'+img_counter+'\"]').parentNode;}catch (e){try{link=document.body.querySelector('img[data-counter=\"0\"]').parentNode;}catch (e){return false;}}doClick(link);}); overlay.addEventListener('click', function(e){overlay.style.display=\"none\";}) document.onkeypress=function(e){e=e || window.event; var charCode=e.keyCode || e.which; switch(charCode){case 27: // Esc overlay.style.display=\"none\"; break; case 34: // right-arrow, Page Down, keypad right, ... case 39: case 54: case 102: e.preventDefault(); doClick(imageDisplay); break; case 33: // left-arrow, Page Up, keypad right, ... case 37: case 52: case 100: e.preventDefault(); document.getElementById('imageDisplay').dataset.counter -=2; // hacky break;}}; </script> </body></html>"
}

private func generateHTMLFile(with text: String, relativePath: String) {
    do {
        try text.write(toFile: "\(currentDirPath)/\(relativePath)", atomically: true, encoding: .utf8)
        print("\(relativePath) generated")
    } catch {
        print(error.localizedDescription)
    }
}

if CommandLine.argc < 2 {
    
    if let features = Feature.features, let listItems = Feature.listItems {
        
        let indexHTMLText = setupIndexHTML(with: listItems)
        generateHTMLFile(with: indexHTMLText, relativePath: "index.html")
        
        for feature in features {
            
            if let products = Product.getProducts(for: feature) {
                
                var productHTMLSnippet = ""
                
                for product in products {
                    var screenshotsHTMLSnippet = ""
                    
                    if let screenshots = Screenshot.getScreenshots(for: feature, product: product) {
                        for screenshot in screenshots {
                            screenshotsHTMLSnippet += screenshot.getHTMLText(for: product, screenshot: screenshot)
                        }
                        productHTMLSnippet += product.getHTMLText(for: product, htmlSnippet: screenshotsHTMLSnippet)
                    }
                }
                let screenshotsHTMLText = setupScreenshotsHTML(for: feature, text: productHTMLSnippet)
                generateHTMLFile(with: screenshotsHTMLText, relativePath: "\(feature.title)/screenshots.html")
            }
            
        }
        
    } else {
        print("There aren't any features")
    }
    
} else {
    print("No arguments need to be passed")
}
