# iOS FEATURE SCREENSHOTS WEBSITE

## Description
This script generates html files to view screenshots of iOS features for EngageFT products

## Requirements
* Swift 4.0 or later installed - to check the Swift version via Terminal, run 'swift --version'

## Features
*  index.html file includes list items of iOS features
*  screenshots.html file includes screenshots of the iOS features across products

## Usage
Run the 'main.swift' script to create html files

In Terminal, run the following commands initially:
    'cd <path-to-Website-directory>'
    'chmod +x main.swift'
    './main.swift>'

If you need to execute the script after the first time, run the following command:
    './main.swift'

Double-click on the index.html file, then navigate the features for EngageFT products
